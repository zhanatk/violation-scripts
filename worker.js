const { Client, logger } = require('camunda-external-task-client-js')
const { Variables } = require('camunda-external-task-client-js')
const BearerTokenInterceptor = require('camunda-external-task-bearer-token-interceptor')
const nodemailer = require('nodemailer')
const axios = require('axios')
const request = require('request')
var moment = require('moment');

// ################ Keycloak ##########################
let bearerTokenInterceptor = null
if (process.env.KEYCLOAK_CLIENT_ID && process.env.KEYCLOAK_CLIENT_SECRET && process.env.KEYCLOAK_BASE_URL && process.env.KEYCLOAK_REALM_ID) {
  bearerTokenInterceptor = new BearerTokenInterceptor({
    clientId: process.env.KEYCLOAK_CLIENT_ID,
    clientSecret: process.env.KEYCLOAK_CLIENT_SECRET,
    baseUrl: process.env.KEYCLOAK_BASE_URL,
    readlmId: process.env.KEYCLOAK_REALM_ID,
  })
}

// ################ Camunda ##########################
const config = { baseUrl: process.env.CAMUNDA_URL, use: logger, interceptors: bearerTokenInterceptor, workerId: 'workflow-messages', interval: 1000 }

// create a Client instance with custom configuration
const client = new Client(config)

// ################ nodemailer #######################
// create reusable transporter object using the default SMTP transport
const transporter = nodemailer.createTransport({
  host: process.env.SMTP_HOST,
  port: process.env.SMTP_PORT,
  secure: process.env.SMTP_SECURE,
  auth: {
    user: process.env.SMTP_AUTH_USER,
    pass: process.env.SMTP_AUTH_PASSWORD,
  },
})

const GoogleSpreadsheet = require('google-spreadsheet')
const creds = require('./client_secret.json')

var list = [
          {
            "no": "1",
            "lfp": "Абдрахманов Еркин Сейсенбекович",
            "companyName": "BMEI ИП Шубаев",
            "cardNo": "136,30475",
            "cardExp": null,
            "time": "07:43:40",
            "action": "Вход  "
          },
          {
            "no": "2",
            "lfp": "Абсугуров Дархан ",
            "companyName": "SICIM PRORVA ",
            "cardNo": "136,37170",
            "cardExp": null,
            "time": "07:48:08",
            "action": "Вход  "
          },
          {
            "no": "3",
            "lfp": "Абсугуров Дархан ",
            "companyName": "SICIM PRORVA ",
            "cardNo": "136,37170",
            "cardExp": null,
            "time": "16:57:52",
            "action": "Выход"
          },
          {
            "no": "4",
            "lfp": "Абубакиров Талгат ",
            "companyName": "BMEI Bi Industrial",
            "cardNo": "253,06248",
            "cardExp": null,
            "time": "07:43:36",
            "action": "Вход  "
          },
          {
            "no": "5",
            "lfp": "Агатаев Диас ",
            "companyName": "BMEI Bi Industrial",
            "cardNo": "189,25955",
            "cardExp": null,
            "time": "07:50:28",
            "action": "Вход  "
          },
          {
            "no": "6",
            "lfp": "Агатаев Диас ",
            "companyName": "BMEI Bi Industrial",
            "cardNo": "189,25955",
            "cardExp": null,
            "time": "16:59:54",
            "action": "Вход  "
          },
          {
            "no": "7",
            "lfp": "Агатаев Диас ",
            "companyName": "BMEI Bi Industrial",
            "cardNo": "189,25955",
            "cardExp": null,
            "time": "16:59:56",
            "action": "Выход"
          },
          {
            "no": "8",
            "lfp": "Абдрахманов Еркин Сейсенбекович",
            "companyName": "BMEI ИП Шубаев",
            "cardNo": "136,30475",
            "cardExp": null,
            "time": "07:53:40",
            "action": "Вход  "
          },
          {
            "no": "9",
            "lfp": "Агатаев Диас ",
            "companyName": "BMEI Bi Industrial",
            "cardNo": "189,25955",
            "cardExp": null,
            "time": "17:59:56",
            "action": "Выход"
          }
]
//---------------------------------------------------------------------------------------------------------------------------
// Конвертация excel файла в JSON и отправка первончального СМС вахтёру с информацией о вахте
client.subscribe('upload-excel', ({ task, taskService }) => {
  let list1 = []
  let date1 = ''
  let times = []
  var shiftDate = moment(new Date()).subtract(1, 'days').format('DD.MM.YYYY')

  request.get({ 
    'url':'http://acs-report.bein.tech/violationEmployees?date='+shiftDate, 
    'auth': {
      'user': 'demo',
      'pass': 'zlBVskoVNJ89irlk'
    }
  }, function(error, response, body){
    list = JSON.parse(body)
    list.forEach((element) => {
      let newE = {
        fio: element["lfp"],
        companyName: element["companyName"],
        action: element["action"]
      }

      let indexE = list1.findIndex((o, i) => {
        if (o.fio == newE.fio && o.companyName == newE.companyName && o.action == newE.action) {
          return true; // stop searching
        }
      })
      if(indexE == -1){
        list1.push(newE)
        times.push(element["time"])
      } else {
        let oldTime = times[indexE].split(":")
        let newTime = element["time"].split(":")
        if(element.action.trim() == "Вход"){
          if(parseInt(oldTime[0]) > parseInt(newTime[0])){ // hour
            times[indexE] = element["time"]
          } else if (parseInt(oldTime[0]) == parseInt(newTime[0])){// minute
            if(parseInt(oldTime[1]) > parseInt(newTime[1])){
              times[indexE] = element["time"]
            } else if (parseInt(oldTime[1]) == parseInt(newTime[1])){//second
              if(parseInt(oldTime[2]) > parseInt(newTime[2])){
                times[indexE] = element["time"]
              }
            }
          }
        } else if(element.action.trim() == "Выход"){
          if(parseInt(oldTime[0]) < parseInt(newTime[0])){ // hour
            times[indexE] = element["time"]
          } else if (parseInt(oldTime[0]) == parseInt(newTime[0])){// minute
            if(parseInt(oldTime[1]) < parseInt(newTime[1])){
              times[indexE] = element["time"]
            } else if (parseInt(oldTime[1]) == parseInt(newTime[1])){//second
              if(parseInt(oldTime[2]) < parseInt(newTime[2])){
                times[indexE] = element["time"]
              }
            }
          }
        }
      }
    })
    for(var i = 0; i < list1.length; i++){
      list1[i].time = times[i]
    }
    const variables = new Variables().setAllTyped({
      employees: {
        value: list1,
        type: 'Json',
      },
      yesterday: {
        value: shiftDate,
        type: 'String',
      },
    })
    taskService.complete(task, variables).then((result) => {
      console.log(`Result: ${result}`)
    })
  })
  // Create a document object using the ID of the spreadsheet - obtained from its URL.
/*  const doc = new GoogleSpreadsheet('1uX3DAVJWHKlwks-P0l-yp1smbM__8_2I1fyT5GRNxkA')
  doc.useServiceAccountAuth(creds, (err) => {
    if (err) {
      taskService.handleFailure(task, 'Authentication Error').then((result) => {
        console.log(`Result: ${result}`)
      })
    } else {
      // Get all of the rows from the spreadsheet.
      doc.getRows(1, (err, rows) => {
        rows.forEach((element) => {
          var shiftDate = moment(new Date()).subtract(1, 'days').format('DD/MM/YYYY')
          try {
            var date = moment(new Date(element.date.substring(3, 5) + '.' + element.date.substring(0, 2) + '.' + element.date.substring(6, 10))).format('DD/MM/YYYY');
            if (shiftDate == date) {
              const x = element.time.split(':')
              if (!(parseInt(x[0]) == 7 && parseInt(x[1]) <= 10) && !(parseInt(x[0]) == 18 && parseInt(x[1]) >= 50)) {
                list1.push({
                  fio: element.fio,
                  subdivision: element.subdivision,
                  resource: element.resource,
                  date: element.date,
                  building: element.building,
                  time: element.time,
                  position: element.position,
                  supervisor: element.supervisor,
                  photos: null,
                  comment: '',
                  picked: 'Two',
                })
              }
            }

            if (list1[0].date !== undefined) {
              date1 = list1[0].date
            }
          } catch(err) {
            console.log('Finished with error. Inspect date value and format.')
          }
          
        })
        console.log(list1)
        
        

        const variables = new Variables().setAllTyped({
          employees: {
            value: list1,
            type: 'Json',
          },
          yesterday: {
            value: date1,
            type: 'String',
          },
        })
        taskService.complete(task, variables).then((result) => {
          console.log(`Result: ${result}`)
        })
      })
    }
  })*/
})


